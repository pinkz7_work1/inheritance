/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.inheritance;

/**
 *
 * @author ripgg
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("A","Blue",0);
        animal.speak();
        animal.walk();
        
        Dog danny = new Dog("Danny","Green");
        danny.speak();
        danny.walk();
        
        Cat camelo = new Cat("Camelo","White");
        camelo.speak();
        camelo.walk();
        
        Duck ed = new Duck("Ed","Black");
        ed.speak();
        ed.walk();
        ed.fly();
        
        Dog dantae = new Dog("Dantae","Green");
        danny.speak();
        danny.walk();
        
        Dog deno = new Dog("Deno","White");
        danny.speak();
        danny.walk();
        
        Dog depito = new Dog("Depito","Black&White");
        danny.speak();
        danny.walk();
        
        Duck eddy = new Duck("Eddy","Brown");
        ed.speak();
        ed.walk();
        ed.fly();
        
        System.out.println("Ed is Animal: "+(ed instanceof Animal));
        System.out.println("Ed is Duck: "+(ed instanceof Duck));
        System.out.println("ed is Object: "+(animal instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        System.out.println("Animal is Animal: "+(animal instanceof Animal));
        
        Animal [] animals={danny, camelo, ed, dantae, deno, depito, eddy};
        for(int i=0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
    }
}
